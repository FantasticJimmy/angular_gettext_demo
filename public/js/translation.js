'use strict';
angular.module('gettext').run(['gettextCatalog', function (gettextCatalog) {
/* jshint -W100 */
    gettextCatalog.setStrings('zh_CN', {"MEAN - A Modern Stack":"MEAN - 潮流框架","Signin":"登入","Signout":"退出","Signup":"注册","This is the home view":"这里是首页"});
/* jshint +W100 */
}]);