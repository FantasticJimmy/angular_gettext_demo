angular.module('mean', ['ngCookies', 'ngResource', 'ngRoute', 'ui.bootstrap', 'ui.route', 'mean.system', 'mean.articles','gettext']);

angular.module('mean.system', ['gettext']);
angular.module('mean.articles', []);