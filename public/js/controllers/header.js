'use strict';
angular.module('mean.system').controller('HeaderController', ['$scope', 'Global','gettextCatalog', function ($scope, Global,gettextCatalog) {
    $scope.global = Global;

    $scope.menu = [{
        "title": "Articles",
        "link": "articles"
    }, {
        "title": "Create New Article",
        "link": "articles/create"
    }];
    
    $scope.isCollapsed = false;
    // Toggle languageb betwee Chinese and English
    $scope.english = true;
    gettextCatalog.setCurrentLanguage('zh_CN');
    var languageFunc = function(english){
      if(english){
        gettextCatalog.setCurrentLanguage("en");
      }else{
        gettextCatalog.setCurrentLanguage("zh_CN");
      }
    };
    $scope.chengeLanguage = function(){
      $scope.english = !$scope.english;
      languageFunc($scope.english);
    };
    languageFunc($scope.english);
}]);